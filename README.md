usage
=====

state_file_storage
------------------

The state_file_storage s3 bucket and dynamodb lock table statuses are checked into this repo as state_fiile_storage.tfstate
Please run the following:
```
  ansible-galaxy install -r requirements.yml -f
  ANSIBLE_ROLES_PATH="../" ansible-playbook -vv -i "127.0.0.1," -c local ./playbook.yml --tags state_file_storage
  cp ./terraform_states/state_file_storage.tfstate ./.ansible_generated_terraform/state_file_storage/terraform.tfstate
  cd .ansible_generated_terraform/state_file_storage
  terraform init
  terraform plan -out=state_file_storage.plan -detailed-exitcode
  terraform apply state_file_storage.plan
  cd -
  cp ./.ansible_generated_terraform/state_file_storage/terraform.tfstate ./terraform_states/state_file_storage.tfstate
```

iam roles
---------
aws iam roles and groups are updated by running:
```
  ANSIBLE_ROLES_PATH="../" ansible-playbook -vv -i "127.0.0.1," -c local ./playbook.yml --tags iam
  cd .ansible_generated_terraform/iam
  terraform init
  terraform plan -out=iam.plan -detailed-exitcode
  terraform apply iam.plan
  cd -
```

deleting iam roles
------------------
```
  ANSIBLE_ROLES_PATH="../" ansible-playbook -vv -i "127.0.0.1," -c local ./playbook.yml --tags iam
  cd .ansible_generated_terraform/iam
  terraform init
  terraform plan -out=iam.plan -detailed-exitcode -destroy
  terraform apply iam.plan
  cd -
```

gpg decrypting passwords and keys
---------------------------------

pick the encrypted value from jenkins artifact from aws-user job then run the following from your laptop:

```
echo '<encrypted value>' | base64 --decode | keybase pgp decrypt
```
